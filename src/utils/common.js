import _ from 'lodash'
export default {


    formatterDate(value, isOnlyDate) {
        if (!value) return ''
        const date = new Date(value)
        const year = date.getFullYear()
        const month = date.getMonth() + 1
        const day = date.getDate()
        const hour = date.getHours()
        const min = date.getMinutes()
        const second = date.getSeconds()
        if (!isOnlyDate) {
            return `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day
                } ${hour < 10 ? '0' + hour : hour}:${min < 10 ? '0' + min : min}:${second < 10 ? '0' + second : second
                }`
        } else {
            return `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day
                }`
        }
    },
    serialize(obj) {
        var query = ''
        var name
        var value
        var subName
        var innerObj
        var i
        var p

        for (name in obj) {
            value = obj[name]

            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    if ((value[i] instanceof Object) || (value[i] instanceof Array)) {
                        innerObj = {}
                        innerObj[name + '[' + i + ']'] = value[i];
                        (p = serialize(innerObj)) && (query += p + '&')
                    } else if (value[i] !== undefined && value[i] !== null && value !== '') {
                        query += encodeURIComponent(name) + '=' + encodeURIComponent(value[i]) + '&'
                    }
                }
            } else if (value instanceof Object) {
                for (subName in value) {
                    innerObj = {}
                    innerObj[name + '.' + subName] = value[subName];
                    (p = serialize(innerObj)) && (query += p + '&')
                }
            } else if (value !== undefined && value !== null && value !== '') {
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&'
            }
        }

        return query.length ? query.substr(0, query.length - 1) : query
    },
    randomNum(min, max) {
        Math.floor(Math.random() * (max - min + 1)) + min
    }

}